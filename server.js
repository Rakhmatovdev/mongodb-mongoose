const express = require("express");
const   mongoose  = require("mongoose");
const Movie = require("./models/movie");

const PORT = 3001;
const URL = "mongodb+srv://Jasurbek:Jasurbek3@cluster0.kd7sllt.mongodb.net/datas";


const app =  express();
app.use(express.json());

mongoose
.connect(URL)
.then(()=>console.log("Connected to MongoDB"))
.catch((error)=> console.log(`DB connection error: ${error}`))

app.listen(PORT, (err) => {
  err ? console.log(err) : console.log(`listening port ${PORT}`);
});
  
  const handleError = (res, error) => {
    res.status(500).json({ error });
  }

  // GET
  app.get('/data', (req, res) => {
    Movie
      .find()
      .sort({ title: 1 })
      .then((data) => {
        res
          .status(200)
          .json(data);
      })
      .catch(() => handleError(res,"Something goes wrong data is not..." ))
  });

//GET by ID
  app.get('/data/:id', (req, res) => {
      Movie
      .findById(req.params.id)
      .then((data) => {
        console.log(data);
        res
          .status(200)
          .json(data);
      })
      .catch(() => handleError(res,"Something goes wrong data item not..." ))
  });

  //DELETE
  app.delete('/data/:id', (req, res) => {
    Movie
      .findByIdAndDelete(req.params.id)
      .then((data) => {
        res
          .status(200)
          .json(data);
      })
      .catch(() => handleError(res,"Something goes wrong no deleted..." ))
  });

  //POST
  app.post("/data",(req,res)=>{
    const movie= new Movie(req.body)
    movie
    .save()
    .then((result) => {
      console.log(result);
      res
        .status(201)
        .json(result);
    })
    .catch(() => handleError(res,"Something goes wrong no posted..." ))
  })

  //PATCH update
  app.patch("/data/:id",(req,res)=>{
    Movie
      .findByIdAndUpdate((req.params.id),req.body)
      .then((result) => {
        console.log(result);
        res
          .status(200)
          .json(result);
      })
      .catch(() => handleError(res,"Something goes wrong..." ))
  })